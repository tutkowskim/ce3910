#ifndef TWI_H_   /* Include guard */
#define TWI_H_

#include <util/twi.h>
#include <avr/io.h>
#include <util/delay.h>

enum commands {start, repeatedStart, stop, sendData, getData, sendAddress};

void TWIinit(void); 				// This function will initialize the TWBR register

uint8_t TWIaction(enum commands command, uint8_t data);// This function is needed by every TWI operation.
													// It performs operations that must be performed for each
 	 	 	 	 	 	 	 	 					// operation, such as clearing TWINT and enabling the
													// TWI system. In addition, it performs any other
 	 	 	 	 	 	 	 	 					// command needed by this particular operation. This
													// command is passed into the function as a uint8_t.
 	 	 	 	 	 	 	 	 					// It the returns the status (TWSR) of that operation

uint8_t TWIread(uint8_t address, uint8_t regNumber); // This function is used to read the contents of any of
 	 	 	 	 	 	 	 	 	 	 	 	 	 // the camera�s registers. It returns the 8-bit contents of the
 	 	 	 	 	 	 	 	 	 	 	 	 	 // specified register.

void TWIwrite(uint8_t address, uint8_t regNumber, uint8_t value);// This function takes the value passed in
 	 	 	 	 	 	 	 	 	 	 	 	 	 	 	 	 // and writes it to the specified register.
																 // It returns nothing.

#endif
