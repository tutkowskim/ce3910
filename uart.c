#include "uart.h"

// Rx Buffer variables
volatile unsigned int charsInBuffer;
volatile char rxBuffer[RX_BUFSIZE];

void uart_init() {
	uint16_t ubrr = SYSCLK / (16.0 * uart_BAUDRATE) - 1;
	UBRRL = ubrr & 0xFF;
	UBRRH = ubrr >> 8;

	/* Enable receiver, transmitter, and receiver interrupt */
	UCSRB = (1 << RXEN) | (1 << TXEN);

	/* Set frame format: 8 data, 1 stop bit, no parity, polling */
	UCSRC = (1 << URSEL) | (0 << USBS) | (0 << UPM1) | (0 << UPM0)
			| (0 << UCSZ2) | (1 << UCSZ1) | (1 << UCSZ0);

	// Set buffer length to 0
	uart_clearBuffer();
}

void uart_putc(const char c) {
	/* Wait for empty transmit buffer */
	while (!( UCSRA & (1 << UDRE)))
		;

	/* Put character into buffer, sends the data */
	UDR = c;
}

char uart_getc(void) {
	/* Wait to receive data */
	while (!(UCSRA & (1 << RXC)))
		;
	char character = UDR;

	// We may not want to put the char in the buffer
	// depending on the current state.
	int placeCharInBuffer = 1;

	if ((charsInBuffer > 0) && (rxBuffer[charsInBuffer - 1] != '\n') // Delete last character in line.
			&& (character == 127 || character == '\b')) // Note: 127 = Delete, 8 = Backspace
			{
		character = 127; 	// Send a delete

		rxBuffer[charsInBuffer - 1] = '\0';
		charsInBuffer = charsInBuffer - 1;
		placeCharInBuffer = 0;
	} else if (charsInBuffer >= RX_BUFSIZE) // Buffer cannot accept anymore input
	{
		character = 07;
		placeCharInBuffer = 0;
	} else if ((charsInBuffer == RX_BUFSIZE - 1) && character != '\n') // Last character is reserved for newline
			{
		character = 07;
		placeCharInBuffer = 0;
	} else if (character == '\t') // Replace tab with space
			{
		character = ' ';
	} else if (character == 13) // Add new line to carriage return
			{
		uart_putc(13);		// Print carriage  return
		character = '\n'; 	// Print and save line feed
	}

	// Place char in buffer
	if (placeCharInBuffer == 1) {
		rxBuffer[charsInBuffer] = character;
		charsInBuffer = charsInBuffer + 1;
	}

	// Echo back character
	uart_putc(character);

	return character;
}

char* uart_bufferToString(char* buffer, unsigned int size) {
	if (charsInBuffer - 1 > size) // String will not fit in buffer
			{
		return '\0';
	}

	for (int i = 0; i < size; ++i) {
		buffer[i] = (i <= charsInBuffer) ? rxBuffer[i] : '\0';
	}

	return buffer;
}

void uart_clearBuffer() {
	for (int i = 0; i < RX_BUFSIZE; ++i) {
		rxBuffer[i] = '\0';
	}
	charsInBuffer = 0;
}

void uart_printString(const char* string) {
	int i = 0;
	while (string[i] != '\0') {
		uart_putc(string[i]);
		i = i + 1;
	}
}

uint8_t uart_charsInBuffer() {
	return charsInBuffer;
}
