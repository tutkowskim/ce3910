#ifndef CAMERA_H_   /* Include guard */
#define CAMERA_H_

#include <inttypes.h>
#include <stdio.h>
#include "uart.h"
#include "twi.h"
#include "servo.h"

#define CAMERA_ADDRESS 0xC0
#define VIEW_FRAME_WIDTH 352
#define VIEW_FRAME_HEIGHT 288
#define HALF_VIEW_FRAME_WIDTH 176
#define HALF_VIEW_FRAME_HEIGHT 144

void cameraInit();							// Set up the camera for operation
uint8_t cameraRead(const uint8_t reg);// Read to one of the camera's registers using TWI
void cameraWrite(const uint8_t reg, const uint8_t value);// Write to one of the camera's registers using TWI
void cameraEchoFrameRow(const uint16_t row);// Echo a specific row of the current frame over UART
void getRowValues(const uint16_t frameRowIndex, uint8_t* frameRowValues);// Value is returned in frameRowValues
void cameraEchoFrame();							// Echo a frame over UART
void cameraCenterImage();

#endif
