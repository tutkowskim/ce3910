#include "camera.h"

uint8_t blackIllumination = 0;
#define blackIlluminationTolerance 0.7

void cameraInit() {
	TWIinit();

	// Reset Camera Defaults
	cameraWrite(0x12, 0x80);

	// Slow camera
	cameraWrite(0x11, 0x0a);

	// Turn off auto white balance
	//cameraWrite(0x12, 0b100000);

	// Set contrast and brightness respectively
	cameraWrite(0x05, 0x10);
	cameraWrite(0x06, 0x60);
	cameraWrite(0x07, 0xcf);

	// Set PD2 (PCLK), PD3 (HREF), PD6 (VSYNC) as inputs with pullup resistors
	DDRD &= ~(1 << PIN2 | 1 << PIN3 | 1 << PIN6);

	// Set PORTA as all inputs
	DDRA = 0x00;
	PORTA = 0xFF;

	// Set HREF (INT1) and VSYNCH (INT2) as rising edge and PCLK (INT0) as falling edge
	MCUCR |= (1 << ISC11 | 1 << ISC10 | 1 << ISC01);
	MCUCR &= ~(1 << ISC00);
	MCUCSR |= (1 << ISC2);
}

uint8_t cameraRead(const uint8_t reg) {
	return TWIread(CAMERA_ADDRESS, reg);
}

void cameraWrite(const uint8_t reg, const uint8_t value) {
	TWIwrite(CAMERA_ADDRESS, reg, value);
}

void cameraEchoFrame() {
	uart_printString("[ ");
	for (uint16_t i = 0; i < VIEW_FRAME_HEIGHT; i++) {
		cameraEchoFrameRow(i);
		uart_printString("; ");
	}
	uart_printString("]");
	uart_printString("\n\r");
}

void cameraEchoFrameRow(const uint16_t row) {
	// Get row
	uint8_t frameRowValues[VIEW_FRAME_WIDTH];
	getRowValues(row, frameRowValues);

	// Echo array over UART
	char outputBuffer[10];
	for (uint16_t i = 0; i < VIEW_FRAME_WIDTH; i++) {
		sprintf(outputBuffer, "%u ", frameRowValues[i]);
		uart_printString(outputBuffer);
	}
}

void getRowValues(const uint16_t frameRowIndex, uint8_t* frameRowValues) {
	// Wait for new frame
	while ((GIFR & (1 << INTF2)) == 0x00);
	GIFR |= (1 << INTF2);

	// Wait for correct row
	for (uint16_t i = 0; i < frameRowIndex; ++i) {
		while ((GIFR & (1 << INTF1)) == 0x00);
		GIFR |= (1 << INTF1);
	}

	// Iterate over columns
	for (uint16_t frameColumnIndex = 0; frameColumnIndex < VIEW_FRAME_WIDTH; ++frameColumnIndex) {
		while ((GIFR & (1 << INTF0)) == 0x00);
		GIFR |= (1 << INTF0);
		frameRowValues[frameColumnIndex] = PINA;
	}
}

void cameraCenterImage() {
	uint32_t top = 0;
	uint32_t bottom = 0;
	uint16_t left = 0;
	uint16_t right = 0;

	// Wait for new frame
		while ((GIFR & (1 << INTF2)) == 0x00);
		GIFR |= (1 << INTF2);

		// Iterate over rows
		for (uint16_t frameRowIndex = 0; frameRowIndex < VIEW_FRAME_HEIGHT; ++frameRowIndex) {
			while ((GIFR & (1 << INTF1)) == 0x00);
			GIFR |= (1 << INTF1);

			// Iterate over columns
			for (uint16_t frameColumnIndex = 0; frameColumnIndex < VIEW_FRAME_WIDTH; ++frameColumnIndex) {
				while ((GIFR & (1 << INTF0)) == 0x00);
				GIFR |= (1 << INTF0);

				if((frameRowIndex >= 5 && frameRowIndex < VIEW_FRAME_HEIGHT-5) && ( frameColumnIndex == HALF_VIEW_FRAME_WIDTH || frameColumnIndex == HALF_VIEW_FRAME_WIDTH-1 || frameColumnIndex == HALF_VIEW_FRAME_WIDTH+1)) {
					if(frameRowIndex < HALF_VIEW_FRAME_WIDTH) {
						bottom += PINA;
					} else {
						top += PINA;
					}
				}

				if( (frameRowIndex == HALF_VIEW_FRAME_HEIGHT || (frameRowIndex == HALF_VIEW_FRAME_HEIGHT-1) || (frameRowIndex == HALF_VIEW_FRAME_HEIGHT+1))) {
					if(frameColumnIndex <  HALF_VIEW_FRAME_HEIGHT) {
						right += PINA;
					} else {
						left += PINA;
					}
				}
			}
		}


	char outputBuffer[100];
	uart_printString(outputBuffer);
	sprintf(outputBuffer,"Top: %u Bottom: %u Left: %u Right: %u  \n\r", (int)top, (int)bottom, (int)left, (int)right);

	// Move Servos
	if (top > bottom) {
		// Black image is on top of frame
		setServoY(getServoY() + 1);
	} else if(top < bottom) {
		// Black image is on bottom of frame
		setServoY(getServoY() - 1);
	}

	if (left > right) {
		// Black image is on left of frame
		setServoX(getServoX() +1);
	} else if(left < right){
		// Black image is on right of frame
		setServoX(getServoX() - 1);
	}
}
