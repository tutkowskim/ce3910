#ifndef SERVO_H_   /* Include guard */
#define SERVO_H_

#include <avr/io.h>
#include <avr/interrupt.h>
#include <inttypes.h>
#include <math.h>

/**
 * Set up the DDR for the servo port
 */
void initServoPort(void);

/**
 * Set up ICR1, OCR1A, OCR1B, TCCR1A and TCCR1B
 */
void initServo(void);

/**
 * Used to provide an absolute x-axis servo position. The number provided
 * is to be a number between 0 and 176. This corresponds to the horizontal
 * resolution of the camera. These values should cause a full range of travel
 * from far left to far right.
 */
void pan(int col);

/**
 * Used to provide an absolute y-axis servo position. The number provided
 * is to be a number between 0 and 144. This corresponds to the vertical
 * resolution of the camera. These values should cause a full range of travel
 * from top to bottom.
 */
void tilt(int row);

int getServoX(void);
void setServoX(int xpos);
int getServoY(void);
void setServoY(int ypos);

#endif
