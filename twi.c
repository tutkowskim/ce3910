#include "twi.h"

#include <stdlib.h>
#define UNUSED_PARAMETER 0

void TWIinit(void) {
	TWSR &= 0b11111100;
	TWBR = 72;
}

uint8_t TWIaction(enum commands command, uint8_t data) {
	switch (command) {
	case repeatedStart:
		TWIaction(stop, data);
		TWIaction(start, data);
		break;
	case start:
		TWCR = (1 << TWINT) | (1 << TWSTA) | (1 << TWEN);
		break;
	case sendAddress:
		/* no break */
	case sendData:
		TWDR = data;
		TWCR = (1 << TWINT) | (1 << TWEN);
		break;
	case getData:
		TWCR = (1 << TWINT) | (1 << TWEN);
		break;
	case stop:
		TWCR = (1 << TWINT) | (1 << TWEN) | (1 << TWSTO);
		break;
	default:
		// Unknown Command
		return -1;
	}

	// Wait for transaction to end
	if (command != stop && command != repeatedStart) {
		while (!(TWCR & (1 << TWINT)))
			;
	}
	_delay_ms(2);
	return TWSR;
}

uint8_t TWIread(uint8_t address, uint8_t regNumber) {
	TWIaction(start, UNUSED_PARAMETER);
	TWIaction(sendAddress, address);
	TWIaction(sendData, regNumber);
	TWIaction(repeatedStart, UNUSED_PARAMETER);
	TWIaction(sendAddress, address + 1);
	TWIaction(getData, UNUSED_PARAMETER);
	uint8_t retVal = TWDR;
	TWIaction(stop, UNUSED_PARAMETER);
	return retVal;
}

void TWIwrite(uint8_t address, uint8_t regNumber, uint8_t value) {
	TWIaction(start, UNUSED_PARAMETER);
	TWIaction(sendAddress, address);
	TWIaction(sendData, regNumber);
	TWIaction(sendData, value);
	TWIaction(stop, UNUSED_PARAMETER);
}
