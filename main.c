#include <inttypes.h>
#include <stdlib.h>
#include <string.h>
#include <stdio.h>			// For sscanf and sprintf#include <avr/interrupt.h>	// For sei()#include <avr/io.h>			// For debugging#include "camera.h"
#include "uart.h"
#include "servo.h"
#include "messages.h"

enum states {
	init, rcvcmd, excmd, trackImage
};
volatile enum states state = init;

void parseCommand(char* cmd) {
	// Create an buffers
	char commandBuffer[100];
	char outputBuffer[100];

	// In case the buffers are never updated
	commandBuffer[0] = '\0';
	outputBuffer[0] = '\0';

	// Set up sscanf
	unsigned int arg1;
	unsigned int arg2;
	sscanf(cmd, "%s %x %x", commandBuffer, &arg1, &arg2);

	// Get a uint8_t pointer, since sscanf doesn't like uint8_t.
	uint8_t* addressPointer = (uint8_t*) arg1;

	if (strcmp(commandBuffer, "rdIO") == 0) // Read from a register
			{
		arg2 = (*addressPointer);
		sprintf(outputBuffer, "Address 0x%x contains value: 0x%x\n\r", arg1,
				arg2);
	} else if (strcmp(commandBuffer, "wrIO") == 0) // Write to a register
			{
		// Write to a register
		(*addressPointer) = arg2;
		sprintf(outputBuffer, "Writing 0x%x into 0x%x.\n\r", arg2, arg1);
	} else if (strcmp(commandBuffer, "tilt") == 0) {
		if (arg1 >= 0x35 && arg1 <= 180) {
			sprintf(outputBuffer, "Tilting by 0x%x.\n\r", arg1);
			tilt((uint8_t) arg1);
		} else {
			sprintf(outputBuffer,
					"Unable to tilt: 0x%x is outside of the operating range.\n\r",
					arg1);
		}
	} else if (strcmp(commandBuffer, "pan") == 0) {
		if (arg1 >= 0 && arg1 <= 180) {
			pan((uint8_t) arg1);
			sprintf(outputBuffer, "Panning by 0x%x.\n\r", arg1);
		} else {
			sprintf(outputBuffer,
					"Unable to pan: 0x%x is outside of the operating range.\n\r",
					arg1);
		}
	} else if (strcmp(commandBuffer, "rdCamReg") == 0) // Read register from camera
			{
		arg2 = cameraRead(arg1);
		sprintf(outputBuffer, "Register 0x%x contains 0x%x.\n\r", arg1, arg2);
	} else if (strcmp(commandBuffer, "wrCamReg") == 0) // Write to camera
			{
		cameraWrite(arg1, arg2);
		sprintf(outputBuffer, "Writing 0x%x into 0x%x.\n\r", arg2, arg1);
	} else if (strcmp(commandBuffer, "snapshot") == 0) // Send Image Snapshot
			{
		cameraEchoFrame();
		return;
	} else if (strcmp(commandBuffer, "track") == 0) // Print help message
			{
		state = trackImage;
		sprintf(outputBuffer, "Tracking Image.\n\r");
	} else if (strcmp(commandBuffer, "help") == 0
			|| strcmp(commandBuffer, "-h") == 0) // Print help message
					{
		uart_printString(helpMessage);
		return;
	} else {
		sprintf(outputBuffer,
				"%s is an unknown command. Type help or -h for help.\n\r",
				commandBuffer);
	}

	uart_printString(outputBuffer);
}

int main(void) {
	int cmdBufferSize = 80;
	char cmdBuffer[cmdBufferSize];

	while (1) {
		switch (state) {
		case init:
			uart_init(); // Set up the UART
			uart_printString(welcomeMessage);
			initServo();
			cameraInit();
			state = rcvcmd;
			sei();
			break;

		case rcvcmd:
			uart_clearBuffer();
			while (uart_getc() != '\n')
				;	// Wait for the buffer to get a new line
			uart_bufferToString(cmdBuffer, cmdBufferSize);
			state = excmd;
			break;

		case excmd:
			parseCommand(cmdBuffer);
			if (state != trackImage) {
				state = rcvcmd;
			}
			break;

		case trackImage:
			cameraCenterImage();
			state = trackImage;
			break;

		default:
			uart_printString("Unknown State!\n\r Entering infinite loop.");
			while (1)
				;
			break;
		}
	}
}
