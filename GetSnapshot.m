% Open Serial Port
serialPort = serial('COM3');
set(serialPort,'BaudRate',250000);
set(serialPort,'InputBufferSize',480000);
set(serialPort,'Timeout',999999);
fopen(serialPort);

% Send Command through Serial Port
fprintf(serialPort,'snapshot\n');
fgetl(serialPort); % Eat command echoed back
snapshotString = fgetl(serialPort);

% Close Serial Port
fclose(serialPort);
delete(serialPort);

% Display Image
imshow(uint8(eval(snapshotString)))