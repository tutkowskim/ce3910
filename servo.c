#include "servo.h"

volatile uint8_t servoX;	// In degrees
volatile uint8_t servoY;	// In degrees

void initServoPort(void) {
	// Set pins to outputs
	DDRD = (1 << PIN4) | (1 << PIN5);

	// Set pins low
	PORTD = (0 << PORTD4) | (0 << PORTD5);
}

void initServo(void) {
	// Set up port
	initServoPort();
	pan(90);
	tilt(90);

	// Set the counter to 0
	TCNT1 = 0;

	// Turn on interupts
	//TIMSK = (1 << OCIE1A) | (1 << OCIE1B);

	//Set TOP
	ICR1 = 20000;

	// Enable Normal mode, set 8 prescaler
	TCCR1A = (1 << COM1A1) | (0 << COM1A0) | (1 << COM1B1) | (0 << COM1B0)
			| (1 << WGM11) | (0 << WGM10);
	TCCR1B = (1 << WGM13) | (0 << WGM12) | (0 << CS12) | (1 << CS11)
			| (0 << CS10);
}

void pan(int col) {
	if (col >= 0 && col <= 180) {
		servoX = col;
		OCR1A = (uint16_t) (20.0 * servoX / 1.8 + 500);
	}
}

void tilt(int row) {
	if (row >= 0x35 && row <= 180) {
		servoY = row;
		OCR1B = (uint16_t) (20.0 * servoY / 1.8 + 500);
	}
}

int getServoX(void) {
	return servoX;
}

void setServoX(int xpos) {
	pan(xpos);
}

int getServoY(void) {
	return servoY;
}

void setServoY(int ypos) {
	tilt(ypos);
}
