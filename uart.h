#ifndef UART_H_   /* Include guard */
#define UART_H_

#include <avr/io.h>
#include <inttypes.h>
#include <stdint.h>


/* Baudrate settings. Refer to datasheet for baud rate error.
 Note also maximun baud rate.
 br = baudrate, fosc = clock frequency in megahertzs */

#define uart_BAUDRATE	1000000L
#define SYSCLK 			16000000L
#define RX_BUFSIZE 		80


/*****************************************
 Function prototypes
*****************************************/

/*****************************************************************************
 Provides: Initializes uart device. Use uart_BAUDRATE macro for argument or
 consult datasheet for correct value.

 Inputs: ubrrl value

 Returns: none
*****************************************************************************/
void uart_init();

/*****************************************************************************
 Provides: Transmit one character. No buffering, waits until previous character
 transmitted.

 Inputs: Character to be transmitted

 Returns: none

*****************************************************************************/
void uart_putc(const char c);

/*****************************************************************************
 Provides: Receive one character.
 * This features a simple line-editor that allows to delete and
 * re-edit the characters entered, until either CR or NL is entered.
 * Printable characters entered will be echoed using uart_putc().
 *
 * Editing characters:
 *
 * \b (BS) or \177 (DEL) delete the previous character
 * \t will be replaced by a single space
 *
 * All other control characters will be ignored.
 *
 * The internal line buffer is RX_BUFSIZE (80) characters long, which
 * includes the terminating \n (but no terminating \0). If the buffer
 * is full (i. e., at RX_BUFSIZE-1 characters in order to keep space
 * for the trailing \n), any further input attempts will send a \a to
 * uart_putc() (BEL character), although line editing is still
 * allowed.
 *
 * Successive calls to uart_getchar() will be satisfied from the
 * internal buffer until that buffer is emptied again.

 Inputs: none

 Returns: character entered by user

*****************************************************************************/
char uart_getc(void);

void uart_clearBuffer();

char* uart_bufferToString(char* buffer, unsigned int size);	// Return the buffer as a string with a terminating 0

void uart_printString(const char* string);

uint8_t uart_charsInBuffer();

 #endif
